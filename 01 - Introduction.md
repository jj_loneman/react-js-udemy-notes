# 1 - Introduction - ReactJS

## 1.1 - Introduction

* Goals:
  * Get a boilerplate project
  * Gain familiarity with React
  * Learn about general data modeling
  * Delve into Redux
* Instructor's info:
  * Email: ste.grider@gmail.com
    * Include project/relevant files if asking for help
  * Twitter: @sg_in_sf
  * Github: github.com/stephengrider

## 1.2 - Github Links

[https://github.com/StephenGrider/ReduxCasts](https://github.com/StephenGrider/ReduxCasts)

## 1.3 - The Purpose of Boilerplate Projects

Modern JavaScript tooling:

![](images/2018-08-28-16-15-23.png)

## 1.6 - A Taste of JSX

* React is a JS library used to produce HTML
* When you write React code, you write individual components or views (interchangeable terms)
  * Components or views are **snippets of code used to produce HTML**
  * A component is a collection of JS functions used to produce HTML

`src/index.js`

```js
// Create a new component. This component should produce some HTML
const App = function() {
  return <div>Hi!</div>;
}
```

* Delete everything from `src` and just create `index.html`
* When saving the file, nothing will be output to the browser

## 1.7 - More on JSX

* The browser can't understand native JSX - webpack and babel will translate it for the browser

```js
const App = function() {
  return <div>Hi!</div>;
}
```

* The above JSX will be transpiled to the following ES5 code:

```js
"use strict";

var App = function App() {
  return React.createElement(
    "div",
    null,
    "Hi!"
  );
};
```

* JSX makes components look a lot cleaner
* Babel repl is available at babeljs.io

## 1.8 - ES6 Import Statements

`index.js`

```js
// Create a new component. This component should produce some HTML
const App = function() {
  return <div>Hi!</div>;
}

// Take this component's generated HTML and put it on the page (in the DOM)
React.render(App);
```

* Running in the console will give **Error: React is not defined**
* JS modules are the concept that all components are siloed
  * The modules won't be used unless we import them

`index.js`

```js
import React from 'react';

// Create a new component. This component should produce some HTML
const App = function() {
  return <div>Hi!</div>;
}

// Take this component's generated HTML and put it on the page (in the DOM)
React.render(App);
```

* Imported `react` library

## 1.9 - ReactDOM vs React

* If you run the existing code, you'll get a console error that says `Warning: React.render is deprecated. Please use ReactDOM.render form require('react-dom') instead`.
* React is diverging into 2 separate libraries:
  * Core React library
    * Knows how to render React components and nest them together, so on
  * ReactDOM
    * Functionality to actually render them to the DOM is in a separate library

`index.js`

```js
import React from 'react';
import ReactDOM from 'react-dom';

// Create a new component. This component should produce some HTML
const App = function() {
  return <div>Hi!</div>;
}

// Take this component's generated HTML and put it on the page (in the DOM)
ReactDOM.render(App);
```

* Still get error message about invalid component element

## 1.10 - Differences Between Component Instances and Component Classes

* When you create a component, you create a **class or type of a component**

```js
const App = function() {
  return <div>Hi!</div>;
}
```

* The above is a type of component - you can have many different instances of `App`
  * `App` is a **class and not an instance** 
  * Think of the function as a factory which produces instances that get rendered to the DOM
  * You need to try to **instantiate your components before you try to render them to the DOM**

JSX:

```js
<div></div>
```

ES5:

```js
"use strict"

React.createElement("div", null);
```

* Whenever you write JSX and you put a component name in a tag (where `<div>` currently is), **the component name is a component class**, but **using it inside of JSX turns it into a component instance**
  * That's the link between component, component class, and component instance
* To instantiate it, you need to put the component in a tag:

JSX:

```js
const App = function() {
  return <div>Hi!</div>;
}

<App />
```

ES5:

```js
"use strict";

var App = function App() {
  return React.createElement(
    "div",
    null,
    "Hi!"
  );
};

React.createElement(App, null);
```

* The `<App />` actually creates the `App` element

```js
ReactDOM.render(<App />);
```

* Still get the error `Target container is not a DOM element`

## 1.11 - Render Targets

* The reason for the error message because we're telling React to render the element, but we're not telling it where to render to
* `ReactDOM.render()` takes a 2nd argument which is the target for where to render the component to

`index.js`

```js
ReactDOM.render(<App />, document.querySelector('.container'));
```

`index.html`

```html
  <body>
    <div class="container"></div>
  </body>
```

Output:

![](images/2018-08-28-17-24-04.png)

## 1.12 - Component Structure

![](images/2018-08-29-10-40-34.png)

Component structure:

* Search bar
* Video player/metadata
* Single video suggestion
* List of video suggestions
* Entire app

## 1.13 - YouTube Search API Signup

* Go to console.developers.google.com and find the `YouTube Data API v3` api: [https://console.developers.google.com/apis/library/youtube.googleapis.com](https://console.developers.google.com/apis/library/youtube.googleapis.com)
* Click **Create Credentials > New API Key**
  * ![](images/2018-08-29-10-50-39.png)
* Create the api key const in `index.html`
* Install the `youtube-api-search` package

```bash
npm i --save youtube-api-search
```

* `--save` means to save the package to `package.json`

## 1.14 - Export Statements

* You must export the component if you want to reference it from another file

`search_bar.js`

```jsx
import React from 'react';

const SearchBar = () => {
  return <input />;
};

export default SearchBar;
```

`index.js`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

import SearchBar from './components/search_bar';

const API_KEY = 'AIzaSyD5Ssc6kRCD8dLFzMU_Y3PCqXmeAF-o6Nw';

// Create a new component. This component should produce some HTML
const App = () => {
  return (
    <div>
      <SearchBar />
    </div>
  );
};

// Take this component's generated HTML and put it on the page (in the DOM)
ReactDOM.render(<App />, document.querySelector('.container'));
```

* In the `return` statement, it's common practice to include everything in parenthesis
* If you didn't have parens, **need to make sure the first tag is on the same line as the return statement, otherwise you'll get an error**

Output:

![](images/2018-08-29-11-09-55.png)

* Empty input field

## 1.15 - Class-Based Components

* Function components
  * A component that takes in input, returns output
* Class-based components
  * When we want the component to be self-aware
  * Every class-based react component must have a `render()` method
  * The `render()` function must return JSX

`index.js`

```jsx
import React from 'react';

class SearchBar extends React.Component {
  render() {
    return <input />;
  }
}

export default SearchBar;
```

* By using ES6 destructuring, you can do this instead:

```jsx
import React, { Component } from 'react';

class SearchBar extends Component {
  ...
```

## 1.16 - Handling User Events

`index.js`

```jsx
import React, { Component } from 'react';

class SearchBar extends Component {
  render() {
    return <input onChange={this.onInputChange} />;
  }

  onInputChange(event) {
    console.log(event);
  }
}

export default SearchBar;
```

* Name the function `onInputChange()` or `handleInputChange()`
* When referring to vanilla HTML event attributes, use `on` then name of event
* Use curly braces when referring to variables in JSX
* The `event` object passed into `onInputChange()` describes the context of what event occurred

Output:

![](images/2018-08-29-11-33-56.png)

* Every time you input something, the function is called
* You could also do this:

  ```jsx
  return <input onChange={event => console.log(event.target.value)} />;
  ```

  and get rid of the event handler function

## 1.17 - Introduction to State

`index.js`

```jsx
  constructor(props) {
    super(props);

    this.state = { term: '' };
  }
```

* State is used to record and react to certain events
* **Whenever a component's state is changed, the component automatically re-renders and also forces all of its children to re-render**
* Need to initialize state in constructor
* **Functional components do not have state**
* All JS classes have function called `constructor()`

## 1.18 - More on State

`index.js`

```jsx
  render() {
    return (
      <div>
        <input
          value={this.state.term}
          onChange={event => this.setState({ term: event.target.value })}
        />
      </div>
    );
  }
```

* `setState()` actually sets the state

## 1.19 - Controlled Components

`index.js`

```jsx
        <input
          value={this.state.term}
          onChange={event => this.setState({ term: event.target.value })}
        />
```

* When you type in the input field, nothing happens
* When you tell `<input>` that its value is provided by `this.state.term`, **we turn it into a controlled component or a controlled form element**
* A controlled component has its value set by state
  * This means its value only ever changes when the state changes
* `this.setState()` causes the component to re-render
  * When it re-renders, the new value of the component is set to `this.state.term`
  * When you try to type something in and `setState()` is called, the value of the input is not changed
  * When the user typed something, they didn't actually change anything, they only triggered an event
* We don't have an imperative flow of data throughout our app
  * You don't say "the user changed something so I need to figure out what the value is"
  * It's much more declarative syntax
  * You just say "the value of the input is equal to the state"
# 4 - Managing App State with Redux

## 4.1 - Reducers

* **A reducer is a function that returns a piece of the application state**
* Because there are many different pieces of state, there are many different reducers

Book Diagram:

![](images/2018-08-30-11-49-13.png)

* 2 different pieces of state:
  * List of books
    * One reducer would be responsible for producing the list of books
  * Currently selected book
    * Another reducer would be responsible for producing this

Another version of the diagram:

![](images/2018-09-04-09-56-30.png)

* Overall, both `books` and `activeBook` reside in the **application's overall state**
* The `book`'s piece of state will be produced by one reducer, called the **books reducer** (encapsulated in the array)
* The `activeBook`'s piece of state will be produced by another reducer, called the **activeBook reducer**
* **Reducers produce the value of our state**
* One-to-one pairing
  * Key of state: value of state (produced by reducer)

`reducers/reducer_books.js`

```jsx
export default () => {
  return [
    { title: 'JavaScript: The Good Parts' },
    { title: 'Harry Potter' },
    { title: 'The Dark Tower' },
    { title: 'Eloquent Ruby' },
  ];
}
```

* To make use of the reducer anywhere else in the project, we need to export it
* If we import this file anywhere else in the project, it'll automatically receive the books reducer
* Steps to utilizing a reducer:
  1. Create the reducer
  1. Wire it into the application

`index.js`

```jsx
import { combineReducers } from 'redux';
import BooksReducer from './reducer_books';

const rootReducer = combineReducers({
  books: BooksReducer
});

export default rootReducer;
```

* Inside `combineReducers()` is the mapping of our state

## 4.2 - Containers - Connecting Redux to React

* **Container**
  * A React component that has a direct connection to the state managed by Redux
* Diagram:
  * ![](images/2018-08-30-11-49-13.png)
  * The data side is Redux
  * The view side is React
  * In the middle "working, usable app" is `react-redux`
    * Forms the bridge between the 2 separate libraries
    * The only time we get that bridge available where we can take a React component and inject state into it is is a component called a container
* Container is also referred to as a **smart component** as opposed to a **dumb component**
  * A dumb component has no direct connection to redux
  * **You would normally put containers into a different directory, `containers` instead of `components`**

`components/book-list.js`

```jsx
import React, { Component } from 'react';

export default class BookList extends Component {
  renderList() {
    return this.props.books.map(book => {
      return (
        <li
          key={book.title}
          className="list-group-item"
        >
          {book.title}
        </li>
      )
    })
  }
  
  render() {
    return (
      <ul className="list-group col-sm-4">
        {this.renderList()}
      </ul>
    );
  }
}
```

## 4.3 - Containers Continued

Diagram:

![](images/2018-09-05-13-43-53.png)

* When do you know when to turn a component into a container?
  * It varies
  * In general, you want the most parent component that cares about a piece of state to be a container
  * `App` doesn't really care about the list of books or active book
    * It just renders `BooksList` and `BookDetail`
  * `BooksList` only cares about the list of books
  * `BookDetail` only cares about the active book
* `BooksList` and `BookDetail` should be containers and `App` should be a component

## 4.4 - Implementation of a Container Class

`containers/book-list.js`

```jsx
import React, { Component } from 'react';
import { connect } from 'react-redux';

class BookList extends Component {
  renderList() {
    return this.props.books.map(book => {
      return (
        <li
          key={book.title}
          className="list-group-item"
        >
          {book.title}
        </li>
      )
    })
  }
  
  render() {
    return (
      <ul className="list-group col-sm-4">
        {this.renderList()}
      </ul>
    );
  }
}

// Could also be written as `function mapStateToProps(state)`
const mapStateToProps = (state) => {
  // Whatever is returned will show up as props
  // inside of BookList
  return {
    books: state.books
  };
}

export default connect(mapStateToProps)(BookList);
```

* `mapStateToProps(state)` takes in the entire application state (all of it)
  * Whatever is returned from the function will show up in `this.props`
* `export default connect(mapStateToProps)(BookList);`
  * `connect()` takes a function and a component and produces a container
  * A container is just a component that is aware of the state by redux
* **Whenever application state changes (in `state` passed to `mapToProps()`), for example if `books` is updated by the server, the container will instantly re-render with the new list of books**

Output:

![](images/2018-09-05-14-00-20.png)

## 4.5 - Containers and Reducers Overview

* Redux serves to display the application state
* Application state is generated by reducer functions
  * `books` reducer just returns an array of books
  * in `index.js`, the global application state will have a key of `books` which has a value of the array of books
* You can promote a component to a container by importing `react-redux` and creating a `mapStateToProps()` function
* Whenever the application state changes, the component will re-render

## 4.6 - Actions and Action Creators

Diagram of a **lifecycle of an action** in a JS app:

![](images/2018-09-05-14-05-55.png)

* Everything in a Redux app is triggered by something (the user clicks something, ajax request finishes, etc.)
* All of those actions can call an action creator
  * An **action creator** is a function that returns an action
    * The object has a `type` that describes the type of action that was just triggered (e.g. `BOOK_SELECTED`)
    * The object can also have some data that further describes the action (e.g. `book: { title: 'Book 2' }`)
  * **The object that's returned is called an action**
* The action is then sent to all reducers inside the app
* Most reducers have `switch` statements that reacts to different types of actions
  * Example `ActiveBook Reducer`:

    ```js
    switch(action.type) {
      case BOOK_SELECTED:
        return action.book;
      default:
        // I don't care about this action, do nothing
        return currentState;
    }
    ```

  * In the example above, `action.book` is the **new piece of state**
* Because the ActiveBook reducer is wired up to the key of `activeBook` in the state, `action.book` will be the new value of the state
  * The state would now be the following:

    ```js
    {
      activeBook: { title: 'Book 2' },
      books: [{ title: 'Dark Tower' }, { title: 'JavaScript' }]
    }
    ```

* Once all the reducers have processed the action and chose to return state or took no action, that newly assembled state gets pumped back into all the different containers
  * `mapStateToProps()` will get re-run in all the different containers
* Then we wait for the user to trigger another event

1. If `Book 2` is clicked, it will call an action creator which returns an object
2. That object is then automatically sent to all the different reducers in the application
3. Reducers can choose, depending on the action, to return a piece of state
4. That newly returned piece of state gets piped into the application state
5. The application state gets pumped back into the application, casuing all components to re-render

## 4.6 - Binding Action Creators

* **Action creator** is just a function that returns an action
* An **action** is just an object that flows through all reducers
* **Reducers** can use that action to produce a particular piece of state

`containers/book-list.js`

```jsx
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectBook } from '../actions/index';
import { bindActionCreators } from 'redux';

class BookList extends Component {
  renderList() {
    return this.props.books.map(book => {
      return (
        <li
          key={book.title}
          className="list-group-item"
        >
          {book.title}
        </li>
      )
    })
  }
  
  render() {
    return (
      <ul className="list-group col-sm-4">
        {this.renderList()}
      </ul>
    );
  }
}

const mapStateToProps = (state) => {
  // Whatever is returned will show up as props
  // inside of BookList
  return {
    books: state.books
  };
}

// Anything returned from this function will end up as props
// on the BookList container
const mapDispatchToProps = (dispatch) => {
  // Whenever selectBook is called, the result should
  // be passed to all of our reducers
  return bindActionCreators({ selectBook }, dispatch);
};

// Promote BookList from a component to a container - it needs to know
// about this new dispatch method, selectBook. Make it available as a prop
export default connect(mapStateToProps, mapDispatchToProps)(BookList);
```

* `bindActionCreators()` is a function to make sure that the action generated by the action creator flows through the reducers
  * Whenever the action gets created, it makes sure it flows through the `dispatch` function
  * The `dispatch()` function receives all the actions and spits them back out to all the different reducers

## 4.7 - Creating an Action

Output when clicking on an item:

![](images/2018-09-05-14-32-32.png)

* Logs the list item clicked on

`actions/index.js`

```jsx
export const selectBook = (book) => {
  // selectBook is an ActionCreator, it needs to return an action,
  // an object with a type property
  return {
    type: 'BOOK_SELECTED',
    payload: book
  };
};
```

* You would normally extract all consts, like `BOOK_SELECTED`, to a types file

## 4.8 - Consuming Actions in Reducers

`reducers/reducer_active_books.js`

```jsx
// 'state' argument is not application state, only the
// state this reducer is responsible for
export default (state = null, action) => {
  switch (action.type) {
    case 'BOOK_SELECTED':
      return action.payload;
    default:
      return state;
  }
};
```

* `state=null` in the function params defaults `state` to `null` if `state` is `undefined,` since reducers can't handle `undefined` objects
* **Whenever the application first boots up, there is no application state defined**
* **You always want to return a "fresh" object, never mutate it in the reducer**
  * An example would be doing something like `state.title = book.title`, then returning state

`containers/book-detail.js`

```jsx
import React, { Component } from 'react';

class BookDetail extends Component {
  render() {
    return (
      <div>
        Book Detail!
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    book: state.activeBook
  };
};

export default connect(mapStateToprops)(BookDetail);
```

`components/app.js`

```jsx
import React, { Component } from 'react';

import BookList from '../containers/book-list';
import BookDetail from '../containers/book-detail';

export default class App extends Component {
  render() {
    return (
      <div>
        <BookList />
        <BookDetail />
      </div>
    );
  }
}
```

Output:

![](images/2018-09-05-14-53-44.png)

## 4.9 - Conditional Rendering

`contianers/book-detail.js`

```jsx
import React, { Component } from 'react';
import { connect } from 'react-redux';

class BookDetail extends Component {
  render() {
    // If the app was just initialized, `book` will be null
    if (!this.props.book) {
      return <div>Select a book to get started.</div>;
    }

    return (
      <div>
        <h3>Details for:</h3>
        <div>
          {this.props.book.title}
        </div>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    book: state.activeBook
  };
};

export default connect(mapStateToProps)(BookDetail);
```

Output:

![](images/2018-09-05-14-59-27.png)

After clicking on a book:

![](images/2018-09-05-15-02-28.png)

## 4.10 - Reducers and Actions Review

* Component state is completely different than application state
* Reducers are in charge of changing application state over time through actions
  * They have the option to return different actions
* Action creators are just functions that return actions
  * Actions are just plain JS objects
  * Can optionally have a payload or other properties
    * The term 'payload' is just convention
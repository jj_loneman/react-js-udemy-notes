# 3 - Modeling Application State

## 3.1 - Foreword on Redux

* [Redux Docs](redux.js.org)

## 3.2 - What is Redux?

* Redux is a predictable state container for JS applications

Example of app layout:

![](images/2018-08-29-15-43-52.png)

Diagram:

![](images/2018-08-29-15-44-21.png)

## 3.3 - More on Redux

* With Redux, everything is stored in one object
* Other tools may have different state objects - flux has different stores, backbone has multiple states

Example app:

![](images/2018-08-29-15-48-38.png)

Diagram:

![](images/2018-08-29-15-49-10.png)

* Redux only cares about current count

## 3.4 - Even More on Redux

Tinder mockup:

![](images/2018-08-29-15-50-37.png)

Diagram:

![](images/2018-08-29-15-51-06.png)
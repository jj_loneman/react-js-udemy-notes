# 2 - Ajax Requests with React

## 2.1 - YouTube Search Response

* Concept of **downwards-data flow**
  * The most parent component should be responsible for fetching data
  * `index.js` is the most parent component we have so far or `App`


`index.js`

```jsx
import YTSearch from 'youtube-api-search';

const API_KEY = 'AIzaSyD5Ssc6kRCD8dLFzMU_Y3PCqXmeAF-o6Nw';

YTSearch({ key: API_KEY, term: 'surfboards' }, (data) => {
  console.log(data);
})
```

* The above outputs this to the console:
  * ![](images/2018-08-29-13-47-11.png)

## 2.2 - Refactoring Functional Components to Class Components

`index.js`

```jsx
    YTSearch({ key: API_KEY, term: 'surfboards' }, (videos) => {
      this.setState({ videos });
    });
```

* `{ videos }` is the same as `{ videos: videos }`

## 2.3 - Props

`props.js`

```jsx
import React from 'react';

const VideoList = (props) => {
  return (
    <ul className="col-md-4 list-group">
      {props.videos.length}
    </ul>
  );
};

export default VideoList;
```

* **Note: if using props in a class, need to use `this.props`**

`index.js`

```jsx
class App extends Component {
  constructor(props) {
    super(props);
    
    this.state = { videos: [] };

    YTSearch({ key: API_KEY, term: 'surfboards' }, (videos) => {
      this.setState({ videos });
    });
  }

  render() {
    return (
      <div>
        <SearchBar />
        <VideoList videos={ this.state.videos } />
      </div>
    );
  }
};
```

* Pass props to child components by specifying custom attribute to component

Output:

![](images/2018-08-29-14-03-09.png)

## 2.4 - Building Lists with Map

`video_list_item.js`

```jsx
import React from 'react';

const VideoListItem = (props) => {
  return <li>Video</li>
};

export default VideoListItem;
```

`video_list.js`

```jsx
const VideoList = (props) => {
  const videoItems = props.videos.map((video) => {
    return <VideoListItem video={video} />
  });

  return (
    <ul className="col-md-4 list-group">
      {videoItems}
    </ul>
  );
};
```

Output:

![](images/2018-08-29-14-08-47.png)

## 2.5 - List Item Keys

* If you don't have a unique key for each item in a list, it doesn't know which one to update, so it updates the whole thing if you don't have keys
* Each `video` object has an `etag` property which is unique:

`video_list.js`

```jsx
  const videoItems = props.videos.map((video) => {
    return (
      <VideoListItem
        key={video.etag}
        video={video}
      />
    );
  });
```

## 2.6 - Video List Items

`video_list_item.js`

```jsx
const VideoListItem = ({video}) => {
  const imageUrl = video.snippet.thumbnails.default.url;

  return (
    <li className="list-group-item">
      <div className="video-list media">
        <div className="media-left">
          <img
            className="media-object"
            src={imageUrl}  
          />
        </div>

        <div className="media-body">
          <div className="media-heading">
            {video.snippet.title}
          </div>
        </div>
      </div>
    </li>
  );
};
```

Output:

![](images/2018-08-29-14-21-37.png)

## 2.7 - Detail Component and Template Strings

`video_detail.js`

```jsx
const VideoDetail = ({video}) => {
  const videoId = video.id.videoId;
  const url = `https://www.youtube.com/embed/${videoId}`;

  return (
    <div classname="video-detail col-md-8">
      <div className="embed-responsive embed-responsive-16by9">
        <iframe
          className="embed-responsive-item"
          src={url}  
        ></iframe>
      </div>
      <div className="details">
        <div>{video.snippet.title}</div>
        <div>{video.snippet.description}</div>
      </div>
    </div>
  );
}
```

## 2.8 - Handling Null Props

* If we add `<VideoDetail>` to our `App` component and test it by providing it with the first video, we won't get anything on the page, instead get this error:
  * ![](images/2018-08-29-14-56-43.png)

`index.js`

```jsx
class App extends Component {
  constructor(props) {
    super(props);
    
    this.state = { videos: [] };

    YTSearch({ key: API_KEY, term: 'surfboards' }, (videos) => {
      this.setState({ videos });
    });
  }

  render() {
    return (
      <div>
        <SearchBar />
        <VideoDetail video={ this.state.videos[0] } />
        <VideoList videos={ this.state.videos } />
      </div>
    );
  }
};
```

* In the time between `YTSearch()` attempting to search, the component attempts to render itself (asynchronous)
  * This means that `this.state.videos` is `undefined`
* Some parent objects can't fetch objects fast enough before the child component renders it
* To fix this, just do a check for the null object

`video_detail.js`

```jsx
const VideoDetail = ({video}) => {
  if (!video) {
    return <div>Loading...</div>;
  }
  ...
```

Output:

* First it displayed the `Loading...` text, then it rendered:

![](images/2018-08-29-15-00-43.png)

## 2.9 - Video Selection

* Usually want to put video spinner on higher level component (don't want like 10 spinners)
* Don't want super nested callbacks, passing `onVideoSelect` from `App` to `VideoListItem`, which is like 3 levels deep
  * Might be confusing for someone else who comes in and they have to do a treasure hunt to see how `onVideoSelect` is getting its data

## 2.10 - Styling with CSS

`src/style/style.css`

```css
.search-bar {
  margin: 20px;
  text-align: center;
}

.search-bar input {
  width: 75%;
}

.video-item img {
  max-width: 64px;
}

.video-detail .details {
  margin-top: 10px;
  padding: 10px;
  border: 1px solid #ddd;
  border-radius: 4px;
}

.list-group-item {
  cursor: pointer;
}

.list-group-item:hover {
  background-color: #eee;
}
```

Output:

![](images/2018-08-29-15-16-34.png)

## 2.11 - Searching for Videos

`index.js`

```jsx
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      videos: [],
      selectedVideo: null
    };

    this.videoSearch('surfboards');
  }
  
  videoSearch(term) {
    YTSearch({ key: API_KEY, term }, (videos) => {
      this.setState({
        videos,
        selectedVideo: videos[0]
      });
    });
  }

  render() {
    return (
      <div>
        <SearchBar onSearchTermChange={term => this.videoSearch(term)} />
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList
          onVideoSelect={selectedVideo => this.setState({selectedVideo})}
          videos={this.state.videos}
        />
      </div>
    );
  }
};
```

`search_bar.js`

```jsx
class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { term: '' };
  }

  render() {
    return (
      <div className="search-bar">
        <input
          value={this.state.term}
          onChange={event => this.onInputChange(event.target.value)}
        />
      </div>
    );
  }

  onInputChange(term) {
    this.setState({term});
    this.props.onSearchTermChange(term);
  }
}
```

* This will execute a new search with every keystroke

## 2.12 - Throttling Search Term Input

`index.js`

```jsx
  render() {
    const videoSearch = _.debounce(term => { this.videoSearch(term) }, 300);
    ...
```

* Now, whenever `videoSearch(term)` is called, it will only be called every 300 ms
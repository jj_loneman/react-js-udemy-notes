# 5 - Intermediate Redux - Middleware

## 5.1 - App Overview and Planning

* Weather forecast browser
  * Search for a city - submit query to 3rd party API for next 5 days
  * For each search, add additional row
* Line charts for temperatures
* Will use redux to handle AJAX requests, not react
  * React should only be used to view components

Diagram:

![](images/2018-09-10-11-13-47.png)

## 5.2 - Component Setup

Diagram:

![](images/2018-09-11-10-53-29.png)

* Search bar
  * Needs to modify application state by dispatching actions
  * Needs to call action creator
  * Needs to talk to redux
  * So should be a **container**

`src/containers/search_bar.js`

```jsx
import React, { Component } from 'react';

export default class SearchBar extends Component {
  render() {
    return (
      <form className="input-group">
        <input />
        <span className="input-group-btn">
          <button type="submit" className="btn btn-secondary">
            Submit
          </button>
        </span>
      </form>
    );
  }
}
```

`src/app.js`

```jsx
import React, { Component } from 'react';

import SearchBar from '../containers/search_bar';

export default class App extends Component {
  render() {
    return (
      <div>
        <SearchBar />
      </div>
    );
  }
}
```

Output:

![](images/2018-09-11-11-00-51.png)

## 5.3 - Controlled Components and Binding Context

`containers/search_bar.js`

```jsx
export default class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { term: '' };
  }

  onInputChange(event) {
    console.log(event.target.value);
    this.setState({ term: event.target.value });
  }

  render() {
    return (
      <form className="input-group">
        <input 
          placeholder="Get a five-day forecase in your favorite cities"
          className="form-control"
          value={this.state.term}
          onChange={this.onInputChange}
        />
        ...
```

* The above will produce the following error when typing into the input field
  * `Uncaught TypeError: Cannot read property 'setState' of undefined`
* Last time, we wrapped the event handler inside a fat arrow funciton
* This time, we just give `onChange` in the `<input>` field the literal function `onInputChange`
* **The problem is that the value of `this` inside `onInputChange` is NOT the same value as the `Component`, `SearchBar`.**
  * The value of `this` will be some mystery context
  * So `setState` doesn't belong to the component
* To fix this:
  * Instead of doing `onChange={(e) => this.onInputChange(e)}`, we will bind the context of `onInputChange`
  * **In the constructor, write `this.onInputChange = this.onInputChange.bind(this);`**
  * This will bind `this` to the function `onInputChange`, then we reassign `this.onInputChange` to the bound function
  * This basically overrides the local method

New constructor:

```jsx
  constructor(props) {
    super(props);

    this.state = { term: '' };

    this.onInputChange = this.onInputChange.bind(this);
  }
```

Output:

![](images/2018-09-11-11-15-33.png)

## 5.4 - Form Elements in React

`containers/search_bar.js`

```jsx
...
  onFormSubmit(event) {
    event.preventDefault();
  }

  render() {
    return (
      <form
        className="input-group"
        onSubmit={this.onFormSubmit}  
      >
...
```

* **We want to prevent the default event handling** of the page reloading when the submit button is pressed - this happens by default in the `<form>` element
* We still want to use a form element because we can either press enter or click the submit button
* We don't have to have different event handlers for when we submit the form

## 5.5 - Working with APIs

* OpenWeatherMap api
  * `api.openweathermap.org/data/2.5/forecast?q={city name},{country code}`
* Sign up so you can get an API key
* Paste the api key in `index.js` in the `actions` directory

`src/actions/index.js`

```jsx
const API_KEY = 'your_api_key';
```

## 5.6 - Introduction to Middleware

![](images/2018-09-11-11-40-47.png)

**Middleware:**

* Middlewares are functions that take an action, and depending on the action's type or payload, the middleware can decide to:
  * Let the action pass through
  * Manipulate the action
  * Log the action
  * Or stop the action
* **Middlewares are basically the gatekeeper**
  * They inspect all actions before sending them to reducers

`src/index.js`

```jsx
import ReduxPromise from 'redux-promise';
...
const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);
```

* The above hooks in middleware to our app

## 5.7 - AJAX Requests with Axios

* We only change our application state through reducers and actions
* We need to dispatch an action (call an action creator) which will be responsible for making the ajax request

Action Types:

* Every action needs to have a type
* It's typical to use constants and reference those throughout the reducers, actions, whatever

`src/actions/index.js`

```jsx
import axios from 'axios';

const API_KEY = 'c30bd9f9e6e70b27b1b76bd6f4112cc6';
const ROOT_URL = `https://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
  const url = `${ROOT_URL}&q=${city},us`;
  const request = axios.get(url);

  return {
    type: FETCH_WEATHER,
    payload: request,
  };
}
```

## 5.8 - Redux-Promise in Practice

`src/containers/search_bar.js`

```jsx
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions/index';

export class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { term: '' };

    this.onInputChange = this.onInputChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onInputChange(event) {
    this.setState({ term: event.target.value });
  }

  onFormSubmit(event) {
    event.preventDefault();

    this.props.fetchWeather(this.state.term);
    this.setState({ term: '' });
  }
  ...
}
// This causes the action to flow down through the middleware
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchWeather }, dispatch);
}

export default connect(null, mapDispatchToProps)(Searchbar);
```

* `connect()` takes in `null` as the first arg because it would normally take in state
  * We understand that redux is maintaining some state, but this container doesn't care about state at all

## 5.9 - Redux-Promise Continued

`src/reducers/reducer_weather.js`

```jsx
export default function(state = null, action)  {
  console.log('Action received', action);

  return state;
}
```

* `action` should have the same payload that's in the action creator, defined below, but that's not the case

`src/reducers/index.js`

```jsx
import { combineReducers } from 'redux';
import WeatherReducer from './reducer_weather';

const rootReducer = combineReducers({
  weather: WeatherReducer,
});

export default rootReducer;
```

`src/actions/index.js`

```jsx
export function fetchWeather(city) {
  const url = `${ROOT_URL}&q=${city},us`;
  const request = axios.get(url);

  console.log('Request: ', request);

  return {
    type: FETCH_WEATHER,
    payload: request,
  };
}
```

* Above, we're returning the promise as the payload
* This is the action creator

Output when loading app:

![](images/2018-09-11-14-32-14.png)

* These are default actions that redux always emits on app startup
* These are bootup actions to assemble some initial state for the app
* The default state is to make sure reducers don't return `undefined`

Output when submitting request:

![](images/2018-09-11-14-32-41.png)

* Get 2 additional logs
  * First is from the action creator (where we logged `Request`)
    * This logs a promise
  * Next is `Action received` which comes from the reducer
* `redux-promise` looks at the `payload` property of the action
  * **If the payload is a promise, redux-promise stops the action entirely**
  * **Once the request finishes, it dispatches a new action of the same type but with a payload of the resolved request**
  * In other words, it unwraps the promise for us because the reducers don't really care about the promise - they care about the data
    * So it stops the action, waits until the promise resolves, and then once it gets the resolved data, it sends that to the reducer as the payload
    * Middleware does an interstitial handling of actions for us
  * This is helpful because if we didn't have `redux-promise`, when we got the data, we would get it as a promise, so in our `switch` statement, we would have to wait until the promise got resolved

Diagram:

![](images/2018-09-11-14-44-03.png)

* **The whole point of this is so that the reducer can get normal data and handle it in a normal way without yucking around with promises**
* This makes code a lot cleaner

## 5.10 - Avoiding State Mutations in Reducers

`reducer_weather.js`

```jsx
switch (action.type) {
    case FETCH_WEATHER:
      return state.push(action.payload.data);
  }
```

* **Don't ever do the above**
  * You don't want to manipulate state directly, you only ever want to set the state through `setState()`
  * This is true with react and is the same with redux
  * Whenever you're inside a reducer, you shouldn't mutate your `state`, you should return a new instance of `state`
* From [StackOverFlow](https://stackoverflow.com/q/48806986/1530072):
* **Mutating state is an anti-pattern in react**
  * React uses a rendering engine which depends on the fact that state changes are **observable**
  * This observation is made by comparing previous state with the next state
  * It will alter the virtual dom with the differences and write changed elements back to the dom
  * When you alter the internal state, **React doesn't know what's changed**
    * Even worse, its notion of the current state is incorrect
    * So the DOM and virtual DOM will become out of sync
  * React uses the same idea to update its store
    * An action can be observed by reducers which calculate the next state of the store
    * Changes are emitted and can be consumed by `react-redux`'s `connect`
* Instead of `Object.assign`, you can use the stage-3 spread syntax:
  * `{...previousState, ...changes}` or `[...previousState, ...changes]`

```jsx
return state.concat([action.payload.data]);
```

* You could instead do `concat()` because it returns a new array

```jsx
return [ action.payload.data, ...state ];
```

* This utilizes the ES6 spread operator which does the same thing as the one before
  * It creates a new array of the new data and upacks the current state

`src/reducers/reducer_weather.js`

```jsx
import { FETCH_WEATHER } from '../actions/index';

export default function(state = [], action)  {
  switch (action.type) {
    case FETCH_WEATHER:
      // Returns a new instance of state because we don't want to
      // mutate the existing state
      return [ action.payload.data, ...state ];
  }

  return state;
}
```

* The final version of the weather reducer

## 5.11 - Building a List Container

* The weather list will need access to the state to render the list of weather data

`src/containers/weather_list.js`

```jsx
function mapStateToProps(state) {
  // 'weather' in state.weather is used because we named the key 'weather'
  // in our reducers in index.js
  return { weather: state.weather };
}
```

* Can rewrite the above function to this:

```jsx
function mapStateToProps({ weather }) {
  return { weather };
}
```

* The entire file:

```jsx
import React, { Component } from 'react';
import { connect } from 'react-redux';

export class WeatherList extends Component {
  render() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th>City</th>
            <th>Temperature</th>
            <th>Pressure</th>
            <th>Humidity</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    );
  }
};

function mapStateToProps({ weather }) {
  return { weather };
}

export default connect(mapStateToProps)(WeatherList);
```

* We always want to export the **connected** version of `WeatherList`, which is why the `connect()` function has the `default`

Output:

![](images/2018-09-11-15-16-38.png)

## 5.12 - Mapping Props to a Render Helper

Diagram of redux state we want:

![](images/2018-09-12-14-58-10.png)

`containers/weather_list.js`

```jsx
export class WeatherList extends Component {
  renderWeather(cityData) {
    const name = cityData.city.name;

    return (
      <tr key={name}>
        <td>{name}</td>
      </tr>
    );
  }

  render() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th>City</th>
            <th>Temperature</th>
            <th>Pressure</th>
            <th>Humidity</th>
          </tr>
        </thead>
        <tbody>
          {this.props.weather.map(this.renderWeather)}
        </tbody>
      </table>
    );
  }
};
```

Output after typing in a couple of cities:

![](images/2018-09-12-15-01-32.png)

## 5.13 - Adding Sparkline Charts

Diagram of state mapping to data for our sparkline charts:

![](images/2018-09-12-15-11-26.png)

## 5.14 - Making a Reusable Chart Component

* Made `Chart` component just a function component instead of a class because we don't need to make any use of state

`src/components/chart.js`

```jsx
import React from 'react';
import { Sparklines, SparklinesLine } from 'react-sparklines';

export default (props) => {
  return (
    <div>
      <Sparklines
        height={120}
        width={180}
        data={props.data}
      >
        <SparklinesLine color={props.color} />
      </Sparklines>
    </div>
  );
};
```

`src/containers/weather_list.js`

```jsx
...
import Chart from '../components/chart';

export class WeatherList extends Component {
  // Renders the weather for a single city (row of data)
  renderWeather(cityData) {
    const name = cityData.city.name;
    const temps = cityData.list.map(weather => weather.main.temp);

    return (
      <tr key={name}>
        <td>
          {name}
        </td>
        <td>
          <Chart data={temps} color="orange" />
        </td>
      </tr>
    );
  }
...
```

Output:

![](images/2018-09-12-15-28-24.png)

## 5.15 - Labeling of Units

`components/weather_list.js`

```jsx
...
import kelvinToFahrenheit from 'kelvin-to-fahrenheit';

export class WeatherList extends Component {
  // Renders the weather for a single city (row of data)
  renderWeather(cityData) {
    const name = cityData.city.name;
    const temps = cityData.list.map(weather => kelvinToFahrenheit(weather.main.temp));
    const pressures = cityData.list.map(weather => weather.main.pressure);
    const humidities = cityData.list.map(weather => weather.main.humidity);

    return (
      <tr key={name}>
        <td>
          {name}
        </td>
        <td>
          <Chart
            data={temps}
            color="orange"
            units="F"
          />
        </td>
        <td>
          <Chart
            data={pressures}
            color="green"
            units="hPa"
          />
        </td>
        <td>
          <Chart
            data={humidities}
            color="black"
            units="%"
          />
        </td>
      </tr>
    );
  }
...
```

* Included `units` as a prop to each `Chart` component
* Also mapped kelvin to fahrenheit for each temperature

`components/chart.js`

```jsx
import _ from 'lodash';
import React from 'react';
import { Sparklines, SparklinesLine, SparklinesReferenceLine } from 'react-sparklines';

function average(data) {
  return _.round(_.sum(data) / data.length);
}

export default (props) => {
  return (
    <div>
      <Sparklines
        svgHeight={120}
        svgWidth={180}
        data={props.data}
      >
        <SparklinesLine color={props.color} />
        <SparklinesReferenceLine type="avg" />
      </Sparklines>
      <div>
        {average(props.data)} {props.units}
      </div>
    </div>
  );
};
```

* Changed `height` prop to `svgHeight` as each chart had different sizes
* Added reference line for average of each chart to be displayed
* Displayed average value and appropriate unit for each chart

Output:

![](images/2018-09-12-15-51-44.png)

## 5.16 - Google Maps Integration

First, some styling:

`src/style/style.css`

```css
td, th {
  /* Align each chart and value in each table cell in the middle */
  vertical-align: middle !important;
  text-align: center !important;
}

.input-group {
  /* Increase the top margin for the search bar */
  margin: 20px 0;
}
```

New output:

![](images/2018-09-12-15-59-11.png)

Now, google maps stuff:

* The Google Maps api is already included in `index.html`:
  `<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAq06l5RUVfib62IYRQacLc-KAy0XIWAVs"></script>`

`components/google_map.js`

```jsx
import React, { Component } from 'react';

class GoogleMap extends Component {
  componentDidMount() {
    new google.maps.Map(this.refs.map, {
      zoom: 12,
      center: {
        lat: this.props.lat,
        lng: this.props.lon
      }
    })
  }

  render() {
    return <div ref="map" />
  }
}

export default GoogleMap;
```

* The `ref` keyword:
  * Makes use of the react reference system where when you assign a value to `ref` in an HTML element, you can reference that element by using `this.refs.{refName}`
  * If you say `<div ref="map">`, you can reference that HTML element with `this.refs.map`
* `componentDidMount()` is one of the lifecycle methods that gets called automatically after the component has been rendered to the screen
* The constructor to `google.maps.Map` takes a reference to an HTML element where we want to render the data to and a configurations object
  * A zoom level of 12 gives us a zoom level of about a city-wide view

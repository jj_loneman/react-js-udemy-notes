# 6 - React Router + Redux Form

## 6.1 - App Overview and Goals

This section will cover the following:

* POSTing data
* Routing
* Loading data from backend api before loading page
* Form validation and posting to remote server and saving record

Diagram for blog post app:

`/` page:

![](images/2018-09-12-16-43-16.png)

* Show a list of posts
* Can add a new post

`/posts/{postId}` page:

![](images/2018-09-12-16-44-55.png)

* Shows a single blog post

`/posts/new` page:

![](images/2018-09-12-16-45-43.png)

* Has a form for creating a new blog post
* Clicking "save" saves it to a backend server and takes user back to home page `/`

## 6.2 - Posts API

Diagram of API:

![](images/2018-09-12-16-48-31.png)

* The instant the user loads up the app, we want to show them a list of blog posts
* Stephen already set up blog api for us to use at [http://reduxblog.herokuapp.com/](http://reduxblog.herokuapp.com/)
* Need to provide a unique custom key for the api (`http://reduxblog.herokuapp.com/api/posts?key=jj123`)

## 6.3 - What React Router Does

`npm install --save react-router-dom@4.2.2`

* `react-router` intercepts changes to the url and decides to display a specific component on the screen

`react-router` diagram:

![](images/2018-09-12-17-00-37.png)

* The `history` package takes the new url, figures out what changed, does some parsing, then passes it to `react-router` library
  * This will show certain components to the screen
* Then `react-router` delegates component viewing to react
* Single-page application
* Always dealing with single-page document and swapping out components

## 6.4 - The Basics of React Router

* The `BrowserRouter` object interacts with the `history` library and decides what to do when the user changes the url
* The `Route` object is the workhorse for all things `react-router`
  * It is a react component that we can render inside any other react component that we can put together inside of our app
  * Provides a route configuration that can say what our url looks like, then show a particular component
  * Provides configuration to `react-router`

`src/index.js`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route } from 'react-router-dom';

import App from './components/app';
import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware()(createStore);

class Hello extends React.Component {
  render() {
    return <div>Hello!</div>;
  }
}

class Goodbye extends React.Component {
  render() {
    return <div>Goodbye!</div>;
  }
}

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <Route
          path="/hello"
          component={Hello}
        />
        <Route
          path="/goodbye"
          component={Goodbye}
        />
      </div>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));
```

Going to `/hello`:

![](images/2018-09-12-17-20-02.png)

Going to `/goodbye`:

![](images/2018-09-12-17-20-21.png)

Going to unknown route:

![](images/2018-09-12-17-21-32.png)

Adding HTML before the `<Route>` element:

```jsx
// ...
ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <h1>Header</h1>
        <Route
          path="/hello"
          component={Hello}
        />
// ...
```

* This will always display `Header` on each route

![](images/2018-09-12-17-23-26.png)

## 6.5 - Route Design

* `PostsIndex` component for `/` route
* `PostsShow` component for `/posts/:id` route
* `PostsNew` component for `/posts/new` route

## 6.6 - Our First Route Definition

* With `react-router`, you don't need a central single component to wrap the application, so you don't need an `App` component

`src/index.js`

```jsx
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route } from 'react-router-dom';

import reducers from './reducers';
import PostsIndex from './components/posts_index';

const createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <Route path="/" component={PostsIndex} />
      </div>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));
```

Output:

![](images/2018-09-13-15-13-49.png)

## 6.7 - State as an Object

Sample diagram (won't be using it):

![](images/2018-09-13-15-15-45.png)

* `posts`
  * Would be an array of posts
* `activePost`
  * Would be a single post object
* We won't be using that idea for the following reasons:
  * The url that the user is visiting is a critical piece of application state
  * When the user visits a different url, we expect the application to update something or show a different component on the screen
  * **The current route the user is on is another piece of state in the app**
  * The `id` in the route url in `/posts/:id` is also the same id that would be in `activePost`
    * So `activePost` would be a duplicate piece of state since we can get it from the route

New diagram:

![](images/2018-09-13-15-22-47.png)

* `posts` would be an object instead of an array
  * The key would be the `id` of the post
  * Makes it easier to find a post

## 6.8 - Back to Redux - Index Action

`npm i --save axios redux-promise`

`src/actions/index.js`

```jsx
import axios from 'axios';

export const FETCH_POSTS = 'fetch_posts';

const ROOT_URL = 'http://reduxblog.herokuapp.com/api';
const API_KEY = '?key=jj123';

export function fetchPosts() {
  const request = axios.get(`${ROOT_URL}/posts${API_KEY}`);

  return {
    type: FETCH_POSTS,
    payload: request
  };
};
```

* `redux-promise` will automatically resolve the `request` promise into real data for when it goes into the reducer

`src/index.js`

```jsx
import promise from 'redux-promise';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);
```

* To incorporate `redux-promise`, need to pass it into `applyMiddleware` function

## 6.9 - Implementing Posts Reducer

* `lodash` library has a function to transform an array into a map
* Can go to [Stephen's online REPL](https://stephengrider.github.io/JSPlaygrounds) as a fiddle

```js
const posts = [
  { id: 4, title: 'Hi' },
  { id: 25, title: 'Bye' },
  { id: 36, title: "How's it going?" }
];

const state = _.mapKeys(posts, 'id');
console.log(state['4']); // Prints { id: 4, title: 'Hi' }
```

## 6.10 - Action Creator Shortcuts

`src/components/posts_index.js`

```jsx
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions';

class PostsIndex extends Component {
  componentDidMount() {
    this.props.fetchPosts();
  }

  render() {
    return (
      <div>
        Posts Index
      </div>
    );
  }
}

export default connect(null, { fetchPosts })(PostsIndex);
```

* The above is completely identical to the `mapStateToProps` function
  * We still have access to `this.props.fetchPosts` in the class
  * **This just asks `connect()` to do that extra step of binding state to props for us**
* There are still times where you want to have a separate function, like if you want to do some computation on how you want to call an action creator

* For the next question, we want to know when we want to call the action creator (calling `fetchPosts`)?
  * We usually want to fetch data after some event occurs, like when the user clicks on something
  * As soon as the component is rendered, we want to fetch the data
* To do this, we want to use `componentDidMount`
  * `componentDidMount` will be called immediately after the component has been shown on the DOM
  * Good function to use if we want to do something one time after component has loaded
  * It doesn't really matter if we call the action creator before or after the component shows up on the screen
  * Fetching our data is asynchronous
  * React doesn't have any concept of not rendering the component until after we do some pre-loading operation
  * React will always eagerly render itself as soon as it can, it won't wait
    * We could use `componentWillMount`, which is called before the component is rendered, but it doesn't really matter because we still need to only call the action creator one time

When we load the webpage, we can inspect the network tab to see the AJAX request:

![](images/2018-09-13-15-56-48.png)

## 6.11 - Rendering a List of Posts

`src/components/posts_index.js`

```jsx
class PostsIndex extends Component {
  componentDidMount() {
    this.props.fetchPosts();
  }

  render() {
    console.log(this.props.posts);

    return (
      <div>
        Posts Index
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { posts: state.posts };
}

export default connect(mapStateToProps, { fetchPosts })(PostsIndex);
```

* We see two console logs
* When we first render the component we called our action creator to fetch our lists of posts
  * So everything renders one time without posts being available
* After AJAX request finishes, our action creator finishes, promise resolves, and component re-renders

New file:

`components/posts_index.js`

```jsx
import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions';

class PostsIndex extends Component {
  componentDidMount() {
    this.props.fetchPosts();
  }

  renderPosts() {
    return _.map(this.props.posts, post => {
      return (
        <li
          className="list-group-item"
          key={post.id}
        >
          {post.title}
        </li>
      );
    });
  }

  render() {
    return (
      <div>
        <h3>Posts</h3>
        <ul className="list-group">
          {this.renderPosts()}
        </ul>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { posts: state.posts };
}

export default connect(mapStateToProps, { fetchPosts })(PostsIndex);
```

Output:

![](images/2018-09-13-16-12-16.png)

## 6.12 - Creating New Posts

Diagram:

![](images/2018-09-13-16-13-57.png)

`src/components/posts_new.js`

```jsx
import React, { Component } from 'react';

class PostsNew extends Component {
  render() {
    return (
      <div>
        PostsNew!
      </div>
    );
  }
}

export default PostsNew;
```

* Created PostsNew component

`src/index.js`

```jsx
// ...
import PostsNew from './components/posts_new';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <Route path="/" component={PostsIndex} />
        <Route path="/posts/new" component={PostsNew} />
      </div>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));
  // ...
```

* Imported `PostsNew` component and added route

Output after going to `/posts/new`:

![](images/2018-09-13-16-18-42.png)

## 6.13 - A React Router Gotcha

* `react-router` fuzzily matches `/` against `/posts/new`
* The `Switch` component from `react-router` utilizes order of routing precedence

New `src/index.js`:

```jsx
// ...
ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <Switch>
          <Route path="/posts/new" component={PostsNew} />
          <Route path="/" component={PostsIndex} />
        </Switch>
      </div>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));
```

New output:

![](images/2018-09-13-16-23-36.png)

## 6.14 - Navigation with the Link Component

* Normally for page navigation, you would use anchor tags
  * Anchor tags do discrete navigation inside the browser, you dont' want to do that in react
* Can use `Link` component from `react-router` library which is similar to using anchor tag

`src/components/posts_index.js`

```jsx
import { Link } from 'react-router-dom';
// ...
  render() {
    return (
      <div>
        <div className="text-xs-right">
          <Link
            className="btn btn-primary"
            to="/posts/new"  
          >
            Add a Post
          </Link>
        </div>
        <h3>Posts</h3>
        <ul className="list-group">
          {this.renderPosts()}
        </ul>
      </div>
    );
  }
```

`/` page:

![](images/2018-09-13-16-31-20.png)

`/posts/new` page after clicking link:

![](images/2018-09-13-16-32-21.png)

## 6.15 - Redux Form

* Redux Form documentation is really good [redux-form.com](redux-form.com)
* `npm i --save redux-form@6.6.3`

`src/reducers/index.js`

```jsx
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import PostsReducer from './reducer_posts';

const rootReducer = combineReducers({
  posts: PostsReducer,
  form: formReducer
});

export default rootReducer;
```

* We need to import the reducer from the `redux-form` library and hook it up to our `combineReducers` call
* Internally, `redux-form` uses our redux instance (or our instance of the redux store) for handling state that is being produced by the form
* `redux-form` basically saves us from having to manually wire up a bunch of action creators
* They recommend importing `reducer` property as `formReducer` alias to avoid name clashing
* **You must assign the `formReducer` value to the `form` key**
  * All other reducers will assume your form is hooked up to `form`

## 6.16 - Setting up Redux Form

Diagram of what Redux Form is doing for us:

![](images/2018-09-13-17-01-42.png)

1. Identify different pieces of state (we have title, categories, contents)
2. For each different piece of state, we will create a "field" component
    1. A "field" component is created by `redux-form` for us
    2. All we have to do is tell "field" what type of input we want to receive from the user (e.g. text input, radio button)
3. Then, at some point in time, the user will change the field input
    1. e.g. Adding text or checking checkbox
4. Redux form will automatically handle changes for us
    1. Do stuff in the `onChange` handler, setting our state, setting the value on the input, which is a lot of boilerplate code
5. User will attempt to submit form
6. If the form is valid, we'll handle form submittal

`src/components/posts_new.js`

```jsx
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

class PostsNew extends Component {
  render() {
    return (
      <form>
        <Field
          name="title"
          component={}
        />
      </form>
    );
  }
}

export default reduxForm({
  form: 'PostsNewForm'
})(PostsNew);
```

* `Field` is a react component automatically wired up to redux form
* `reduxForm` is a function which is very similar to the `connect` helper we've been using from `react-redux`
  * Inside `src/reducers/index.js`, we hooked up the `formReducer`, which means that `redux-form` is being integrated somehow into the redux side of our app
  * The `reduxForm` helper in our `PostsNew` component allows our component to communicate with that additional reducer we just wired in
  * **Allows our component to talk to the redux store**
* Inside `export default reduxForm`:
  * The `form` property defines the **name of the form**
    * The string **must be unique**
    * This is because we can show multiple forms at the same time, can't have any name clashing
    * Because then it might try to merge state from multiple forms into one form
* Inside `<Field>` element
  * `name` represents the name of the form field
  * `component` represents the function or component to be used to display the `Field` component

## 6.17 - The Field Component

Diagram:

![](images/2018-09-17-16-20-12.png)

* There is an issue with the `Field` component
  * By default, it knows how to communicate with `redux-form`
    * This is helpful because it saves us from having to wire up different event handlers, action creators, etc.
  * The shortcoming is that it doesn't know how to produce some amount of JSX to show on the screen to the users
  * **In other words, it doesn't know how to show itself on the screen**
* The `component` property will render jsx for us
* The `Field` component really just keeps track of data

`src/components/posts_new.js`

```jsx
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

class PostsNew extends Component {
  renderTitleField(field) {
    return (
      <div>
        <input
          type="text"
          {...field.input}
        />
      </div>
    )
  }

  render() {
    return (
      <form>
        <Field
          name="title"
          component={this.renderTitleField}
        />
      </form>
    );
  }
}

export default reduxForm({
  form: 'PostsNewForm'
})(PostsNew);
```

* Remember, no parenthesis on `this.renderTitleField` in `component` prop for `Field`
  * This is because `Field` will call the function at some point, not us
* The `field` object (in `renderTitleField()`) contains some event handlers that we need to wire up to render some jsx that we're returning
  * It allows the `Field` component to know how to respond to changes in the `<input>` element in `renderTitleField()`
  * `field.input` is an object which contains a bunch of different event handlers and props
    * Like `onChange`, `onBlur`, etc.
    * It also has the value of `input`
  * The `...` in `...field.input` unpacks the object and assigns all of its properties to that of `<input>`

Output:

![](images/2018-09-17-16-33-21.png)

## 6.18 - Generalizing Fields

`src/components/posts_new.js`

```jsx
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

class PostsNew extends Component {
  renderField(field) {
    return (
      <div className="form-group">
        <label>
          {field.label}
        </label>
        <input
          className="form-control"
          type="text"
          {...field.input}
        />
      </div>
    )
  }

  render() {
    return (
      <form>
        <Field
          label="Title"
          name="title"
          component={this.renderField}
        />
        <Field
          label="Tags"
          name="tags"
          component={this.renderField}
        />
        <Field
          label="Post Content"
          name="content"
          component={this.renderField}
        />
      </form>
    );
  }
}

export default reduxForm({
  form: 'PostsNewForm'
})(PostsNew);
```

* The `renderField` function dynamically creates a field
  * Any prop defined in the `Field` element is passed into the `field` argument to `renderField`
  * `label` could've been called `labelToShow` or `fieldName` or something
* State is not shared between each `Field` component, the `renderField` function creates new JSX but each component accesses different pieces of state

Output:

![](images/2018-09-17-16-42-54.png)

## 6.19 - Validating Forms

`src/components/posts_new.js`

```jsx
// ...
function validate(values) {
  const errors = {};

  // Validate the inputs from 'values'
  if (!values.title) {
    errors.title = "Enter a title!";
  }
  if (!values.categories) {
    errors.categories = "Enter some categories!";
  }
  if (!values.content) {
    errors.content = "Enter some content!";
  }

  // If errors is empty, the form is fine to submit
  return errors;
}

export default reduxForm({
  validate,
  form: 'PostsNewForm'
})(PostsNew);
```

The `validate()` function:

* `validate()` will be called at some point automatically during the form's lifecycle (most likely whenever the user tries to submit the form)
  * Takes a single argument, which is by convention called `values`
  * `values` is an object which contains all the different values that the user has entered into the form
* If `validate` returns an empty object, then the form is fine to submit
  * Otherwise, the `errors` object will have some properties that contain the form errors

## 6.20 - Showing Errors to Users

`src/components/posts_new.js`

```jsx
// ...
  renderField(field) {
    return (
      <div className="form-group">
        <label>
          {field.label}
        </label>
        <input
          className="form-control"
          type="text"
          {...field.input}
        />
        {field.meta.error}
      </div>
    )
  }
// ...
```

* `field.meta.error`
  * Automatically added to `field` object from the `validate()` function
  * The property names for each `Field` component need to match property in `errors` object defined in `validate()` function

## 6.21 - Handling Form Submittal

`src/components/posts_new.js`

```jsx
// ...
  onSubmit(values) {
    console.log(values);
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <Field
          label="Title"
          name="title"
          component={this.renderField}
        />
        <Field
          label="Categories"
          name="categories"
          component={this.renderField}
        />
        <Field
          label="Post Content"
          name="content"
          component={this.renderField}
        />
        <button
          type="submit"
          className="btn btn-primary"
        >
          Submit
        </button>
      </form>
    );
  }
// ...
```

* Just like how at the bottom of the file we have `reduxForm()()`, we used the `connect()()` helper to add some additional properties to our component
  * When we use the `reduxForm` helper, it adds a ton of additional properties to our form
  * When we do `this.props.handleSubmit` in our `render()` function, that prop is given to use by `redux-form`
* `redux-form` is just responsible for handling the state and validation of our form
  * It is not responsible for something like taking data and saving it or making POST request or something
* **If the form is good**, then we will call the callback `this.onSubmit` in the `<form>` element
  * `this.onSubmit` is a callback that will be executed in some different context outside of our component, so to make sure we have correct access to the correct `this`, we bind it to the current component

Output:

![](images/2018-09-17-17-22-31.png)
![](images/2018-09-17-17-22-40.png)

## 6.22 - Form and Field States

Small issue - we get error messages on form load:

![](images/2018-09-17-17-23-13.png)

* The validation runs again with every keystroke we make

Three states of an input field:

![](images/2018-09-17-17-25-33.png)

* Pristine
* Touched
  * We only want to display error message if the user touched the field and moved away
* Invalid

Now error message only displays when user touched field:

![](images/2018-09-17-17-27-17.png)

## 6.23 - Conditional Styling

`src/components/posts_new.js`

```jsx
// ...
  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger' : ''}`;

    return (
      <div className={className}>
        <label>
          {field.label}
        </label>
        <input
          className="form-control"
          type="text"
          {...field.input}
        />
        <div className="text-help">
          {touched ? error : ''}
        </div>
      </div>
    )
  }
// ...
```

Output after blurring from title field:

![](images/2018-09-17-17-34-56.png)

## 6.24 - More on Navigation

`src/components/posts_new.js`

```jsx
// ...
        <button
          type="submit"
          className="btn btn-primary"
        >
          Submit
        </button>
        <Link
          to="/"
          className="btn btn-danger"
        >
          Cancel
        </Link>
      </form>
// ...
```

`src/style/style.css`

```css
form a {
  margin-left: 5px;
}
```

Output:

![](images/2018-09-17-17-40-46.png)

## 6.25 - Create Post Action Creator

`src/components/posts_new.js`

```jsx
import { connect } from 'react-redux';
import { createPost } from '../actions';
// ...

  onSubmit(values) {
    this.props.createPost(values);
  }

// ...

export default reduxForm({
  validate,
  form: 'PostsNewForm'
})(
  connect(null, { createPost })(PostsNew)
);
```

* This is how we can layer up helper functions
* `connect` returns a react component, which is what `reduxForm` takes in as 2nd arg
* In `onSubmit` we call the action creator

`src/actions/index.js`

```jsx
// ...
export function createPost(values) {
  const request = axios.post(`${ROOT_URL}/posts${API_KEY}`, values);

  return {
    type: CREATE_POST,
    payload: request
  }
};
```

* Currently, for the `request` object, which presumably contains a post, we can't just take the `values` object and put it directly into state array because it doesn't have an `id` to associate with it

`src/reducers/reducer_posts.js`

```jsx
import _ from 'lodash';
import { FETCH_POSTS } from '../actions';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_POSTS:
      return _.mapKeys(action.payload.data, 'id');
    default:
      return state;
  }
};
```

* Ideally, we want to get the post that's returned from the api and insert it into our `state` object

The form to submit:

![](images/2018-09-17-17-54-11.png)

Evidence of POST request firing (first request):

![](images/2018-09-17-17-55-12.png)

* `OPTIONS` request method just means that we are making a CORS request
  * CORS stands for "cross-origin-resource-sharing"
  * It is a security feature that is inside the browser that prevents malicious code from making requests to other domains

Second request:

![](images/2018-09-17-17-56-51.png)
![](images/2018-09-17-17-57-01.png)

## 6.26 - Navigation through Callbacks

Flow:

![](images/2018-09-17-17-59-00.png)

* Right now, when the user clicks "submit" we don't take them back to the home page
* We don't want to use `Link` tag be created that the user has to click on to go back home, we want to programatically navigate the user back to the homepage after form is submitted
* We want to wait for the post to be created before we go back home

`src/actions/index.js`

```jsx
export function createPost(values, callback) {
  const request = axios.post(`${ROOT_URL}/posts${API_KEY}`, values)
    .then(() => callback());

  return {
    type: CREATE_POST,
    payload: request
  }
};
```

`src/components/posts_new.js`

```jsx
// ...
  onSubmit(values) {
    this.props.createPost(values, () => {
      this.props.history.push('/');
    });
  }
// ...
```

* The new flow is, we create a post (in `PostsNew`), wait for the response (in action creator), then call callback function (in `PostsNew`) which redirects us to home

## 6.27 - The PostsShow Component

`src/components/posts_show.js`

```jsx
import React, { Component } from 'react';

class PostsShow extends Component {
  render() {
    return (
      <div>
        Posts Show!
      </div>
    );
  }
}

export default PostsShow;
```

`src/index.js`

```jsx
// ...
        <Switch>
          <Route path="/posts/new" component={PostsNew} />
          <Route path="/posts/:id" component={PostsShow} />
          <Route path="/" component={PostsIndex} />
        </Switch>
// ...
```

* **We want to make sure that `:id` route is below `/posts/new` because `:id` is the wildcard token**

Navigating to `/posts/123`:

![](images/2018-09-17-18-15-59.png)

## 6.28 - Receiving New Posts

Diagram of state initialization:

![](images/2018-09-17-18-17-39.png)

* If the user goes to `/`, they'll be taken to `PostsIndex` component
  * It will call the action creator to fetch the big list of posts
  * Ends up with a `posts` piece of application state that contains maybe posts 1, 2, 3
* If the user goes to `/posts/2`, they'll be taken to `PostsShow` component
  * When user goes to a different component, we don't want to fetch big list of posts again, we only want to fetch post 2
  * The `PostsShow` component needs to fetch data for the user when the user visits it
* **You should always assume that the user can use any method to enter your application**

`src/actions/index.js`

```jsx
export const FETCH_POST = 'fetch_post';
// ...
export function fetchPost(id) {
  const request = axios.get(`${ROOT_URL}/posts/${id}${API_KEY}`);

  return {
    type: FETCH_POST,
    payload: request
  }
}
```

`src/reducers/reducer_post.js`

```jsx
import _ from 'lodash';
import { FETCH_POSTS, FETCH_POST } from '../actions';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_POST:
      return { ...state, [action.payload.data.id]: action.payload.data };
    case FETCH_POSTS:
      return _.mapKeys(action.payload.data, 'id');
    default:
      return state;
  }
};
```

ES5 way:

```jsx
const post = action.payload.data;
const newState = { ...state,  };
newState[post.id] = post;
return newState;
```

ES6 way:

```jsx
return { ...state, [action.payload.data.id]: action.payload.data };
```

* Inside the square brackets is key interpolation
* We create a new key/value on the object
* Over time, as the user starts to access more `PostsShow` routes, it adds it to the state

## 6.29 - Selecting from OwnProps

`src/components/posts_show.js`

```jsx
class PostsShow extends Component {
  componentDidMount() {
    // this.props.match is provided by react-router
    const { id } = this.props.match.params;
    this.props.fetchPost(id);
  }

  render() {
    return (
      <div>
        Posts Show!
      </div>
    );
  }
}

// 'ownProps' is the object that's going to 'this' component
// ownProps === this.props
function mapStateToProps({ posts }, ownProps) {
  return { post: posts[ownProps.match.params.id] };
}

export default connect(mapStateToProps, { fetchPost })(PostsShow);
```

* `params` lists all the wildcard tokens that exist within the url
* You can use `mapStateToProps` to calculate something or do some intermediate step when getting state
  * In that function, we only want to get one post from the overall state, not the entire list of posts

## 6.30 - Data Dependencies

`src/components/posts_show.js`

```jsx
// ...
class PostsShow extends Component {
  componentDidMount() {
    // this.props.match is provided by react-router
    const { id } = this.props.match.params;
    this.props.fetchPost(id);
  }

  render() {
    const { post } = this.props;

    if (!post) {
      return <div>Loading...</div>;
    }

    return (
      <div>
        <h3>
          {post.title}
        </h3>
        <h6>
          Categories: {post.categories}
        </h6>
        <p>
          {post.content}
        </p>
      </div>
    );
  }
}
// ...
```

* We need to check to see if `post` is undefined first because the component will re-render once it actually fetches the post and assigns it something

Shows "loading" when submitting request for post:

![](images/2018-09-17-18-41-54.png)

Shows actual post after loaded:

![](images/2018-09-17-18-42-54.png)

## 6.31 - Caching Records

`src/components/posts_show.js`

```jsx
// ...
class PostsShow extends Component {
  componentDidMount() {
    // If you didn't want to re-fetch data every time this is called
    // you could wrap the below code in an if statement:
    // if (!this.props.post)

    // this.props.match is provided by react-router
    const { id } = this.props.match.params;
    this.props.fetchPost(id);
  }

  render() {
    const { post } = this.props;

    if (!post) {
      return <div>Loading...</div>;
    }

    return (
      <div>
        <Link
          to="/"
          className="btn btn-primary"
        >
          Back to Index 
        </Link>
        <h3>
          {post.title}
        </h3>
// ...
```

* Added `Back to Index` button

`src/components/posts_index.js`

```jsx
// ...
  renderPosts() {
    return _.map(this.props.posts, post => {
      return (
        <li
          className="list-group-item"
          key={post.id}
        >
          <Link
            to={`/posts/${post.id}`}
          >
            {post.title}
          </Link>
        </li>
      );
    });
  }
// ...
```

`PostsShow` component:

![](images/2018-09-17-18-52-25.png)

`PostsIndex` component:

![](images/2018-09-17-18-52-40.png)

* Each post link is now clickable

## 6.32 - Deleting a Post

`src/actions/index.js`

```jsx
export const DELETE_POST = 'delete_post';

// ...

export function deletePost(id, callback) {
  const request = axios.delete(`${ROOT_URL}/posts/${id}${API_KEY}`)
    .then(() => callback());

  // We don't need to return the whole post when we delete it, so it makes
  // sense to just return the id of the post
  return {
    type: DELETE_POST,
    payload: id
  }
}
```

`src/components/posts_show.js`

```jsx
import { fetchPost, deletePost } from '../actions';
// ...

  onDeleteClick() {
    // Prefer to use this over this.props.post.id because `post` might
    // not be defined but `this.props.match.params` is
    const { id } = this.props.match.params;
    
    this.props.deletePost(id, () => {
      this.props.history.push('/');
    });
  }

  render() {
    const { post } = this.props;

    if (!post) {
      return <div>Loading...</div>;
    }

    return (
      <div>
        <Link
          to="/"
          className="btn btn-primary"
        >
          Back to Index
        </Link>
        <button
          className="btn btn-danger pull-xs-right"
          onClick={this.onDeleteClick.bind(this)}
        >
          Delete Post
        </button>

// ...
```

* Added `onDeleteClick()` function
* Imported `deletePost` from action creator
* Bound `onClick` of delete button to delete function

Output:

![](images/2018-09-17-19-07-05.png)

Index page after clicking button:

![](images/2018-09-17-19-07-22.png)

## 6.33 - Wrapup

* With our current approach, when we click on delete post button, whenever the delete request is completed, we navigate back to our big list of posts
  * When we do that, our big list of posts still include the deleted one
  * So we do the follow-up request to fetch the new list of posts
* With this, we're really exploiting a technicality to get around our state management
  * We should update our state every time a post is deleted
  * This ensures that the ui is very snappy

`src/reducers/reducer_posts.js`

```jsx
import _ from 'lodash';
import { FETCH_POSTS, FETCH_POST, DELETE_POST } from '../actions';

export default function(state = {}, action) {
  switch (action.type) {
    case DELETE_POST:
      return _.omit(state, action.payload);
// ...
```

* `_.omit()` takes a key (`action.payload`) and omits it from that object and returns a new object

Overview of what we did:

* We made use of application routing in `src/index.js`
* Made use of `react-router` with action creators
  * Passing around callbacks to action creators (like `onDeleteClick()` in `posts_show.js`) is a good way to take care of navigation after action has finished exeucting
* `mapStateToProps()` is a good place to do intermediate steps when getting state
* In reducer (`reducer_posts.js`), we made use of `_.mapKeys()` to make an easy lookup of records simple
  * It was easy to manipulate state because it's an object instead of an array

## 6.34 - Bonus

Other videos:

### React

* [Modern React with Redux](https://www.udemy.com/react-redux/?couponCode=4MORE1234)
* [Node with React: Fullstack Web Development](https://www.udemy.com/node-with-react-fullstack-web-development/?couponCode=4MORE1234)
* [The Complete React Native and Redux Course](https://www.udemy.com/the-complete-react-native-and-redux-course/?couponCode=4MORE1234)
* [React Native: Advanced Concepts](https://www.udemy.com/react-native-advanced/?couponCode=4MORE1234)
* [GraphQL With React: The Complete Developer's Guide](https://www.udemy.com/graphql-with-react-course/?couponCode=4MORE1234)
* [Server Side Rendering with React and Redux](https://www.udemy.com/server-side-rendering-with-react-and-redux/?couponCode=4MORE1234)
* [Advanced React and Redux](https://www.udemy.com/react-redux-tutorial/?couponCode=4MORE1234)

### General JS

* [Node JS: Advanced Concepts](https://www.udemy.com/advanced-node-for-developers/?couponCode=4MORE1234)
* [ES6 Javascript](https://www.udemy.com/javascript-es6-tutorial/?couponCode=4MORE1234)
* [Webpack 2: The Complete Developer's Guide](https://www.udemy.com/webpack-2-the-complete-developers-guide/?couponCode=4MORE1234)
* [MongoDB with NodeJS](https://www.udemy.com/the-complete-developers-guide-to-mongodb/?couponCode=4MORE1234)
* [Electron for Desktop Apps](https://www.udemy.com/electron-react-tutorial/?couponCode=4MORE1234)

### Interview Prep

* [The Coding Interview Bootcamp: Algorithms + Data Structures](https://www.udemy.com/coding-interview-bootcamp-algorithms-and-data-structure/?couponCode=4MORE1234)

### Other Languages

* [Ethereum and Solidity: The Complete Developer's Guide](https://www.udemy.com/ethereum-and-solidity-the-complete-developers-guide/?couponCode=4MORE1234)
* [Go: The Complete Developer's Guide (Golang)](https://www.udemy.com/go-the-complete-developers-guide/?couponCode=4MORE1234)
* [Elixir with Phoenix](https://www.udemy.com/the-complete-elixir-and-phoenix-bootcamp-and-tutorial/?couponCode=4MORE1234)